<?php

return [
    'payload_dir' => env('PAYLOAD_PATH', storage_path('new')),
    'pending_dir' => env('PENDING_PATH', storage_path('pending')),
    'ready_dir' => env('READY_PATH', storage_path('ready')),
    'interval_limit' => env('INTERVAL_LIMIT_SEC', 200),
    'live_period' => env('FILE_LIVE_PERIOD_SEC', 1),
];
