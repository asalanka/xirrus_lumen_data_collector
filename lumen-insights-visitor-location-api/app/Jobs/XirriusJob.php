<?php

namespace App\Jobs;

use ChannelLog, Exception;
use App\Http\Traits\BaseTrait;
use Illuminate\Contracts\Queue\ShouldQueue;

class XirriusJob extends Job implements ShouldQueue
{

    use BaseTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $request;
    protected $ln;
    protected $ld;

    public function __construct($csvData)
    {   
        $this->request = $csvData;
        $this->ln = $csvData['ln'];
        $this->ld = $csvData['ld'];
        
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
           if ($this->ld) {
                     ChannelLog::write('event','Xirrius : data',[$this->request]);
                    ChannelLog::write('event','Writing response in to the file!');

                    $this->writeOutput($this->request, trim( $this->ln,'"'));
                    return response()->json($this->request);
            }else{
                     ChannelLog::write('event','Xirrius : missing post: (ld) parameter');
            }

            ChannelLog::write('event', 'Request completed');
        }catch(Exception $e){
           
             ChannelLog::write('event', $e->getMessage());
        }  
    }
}
