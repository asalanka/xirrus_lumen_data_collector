<?php

namespace App\Jobs;

use Exception;
use ChannelLog;
use Carbon\Carbon;
use App\FailedJob;
use App\Http\Traits\BaseTrait;
use Illuminate\Contracts\Queue\ShouldQueue;


class CapturePayload extends Job implements ShouldQueue
{

    use BaseTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $request;
    protected $ld;
    protected $location;
    protected $file_name;

    public function __construct($csvData, $location, $file_name)
    {
        $this->request = preg_replace('/\s+/S', "", $csvData);
        $this->location = snake_case(strtolower(str_replace('%20', '_', $location)));
        $this->file_name = $file_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {

            $jsonData = array();
            $successful = false;


            if ($this->request) {
                //ChannelLog::write('event','Xirrius : data',[$this->request]);
                ChannelLog::write('event', 'Writing response in to the file!');

                ChannelLog::write('event', 'payload request completed');
                $filename = $this->file_name;

                ChannelLog::write('event', $filename);
                $tempPath = config('directory.payload_dir') . '/' . $filename;
                ChannelLog::write('event', $tempPath);

                # open the temporary json payload file to write to
                if (!empty($tempPath)) {

                    //Decoding to verify parameters
                    $xirriusRequest = json_decode($this->request, true);

                    //ChannelLog::write("event", time() . ' Xirrius : Writing Json content to the file -' . $this->request);


                    if (!isset($xirriusRequest['lt']) && !isset($xirriusRequest['id'])) {

                        //If then get the orginal json request note: Orginal request comes as a parameterized request
                        $pairs = explode("&", $this->request);
                        $vars = array();

                        foreach ($pairs as $pair) {
                            $nv = explode("=", $pair);
                            $name = urldecode($nv[0]);
                            $value = urldecode($nv[1]);
                            $vars[$name] = $value;
                        }

                        /**
                         **assign Json decoded result to var for preparing a propper Json bject
                         */

                        $jsonData['lh'] = array(trim($vars['lh'], '"'));
                        $jsonData['ln'] = array(trim($this->location, '"'));
                        $jsonData['ld'] = array(trim($vars['ld'], '"'));

                        ChannelLog::write('event', time() . ' - Json payload type identified : Xirrius presence');

                        $successful = file_put_contents($tempPath, json_encode($jsonData));

                    } else {

                        /**
                         **assign Json decoded result to var for preparing a propper Json bject
                         */

                        $jsonData['ln'] = array(trim($this->location, '"'));
                        $jsonData['xps_ld'] = $xirriusRequest;

                        if (isset($xirriusRequest['lt']) && !empty($xirriusRequest['lt'])) {

                            ChannelLog::write('event', time() . ' - Json payload type identified : Xirrius Geo Location');

                            $successful = file_put_contents($tempPath, json_encode($jsonData));
                        } else {
                            ChannelLog::write('event', time() . ' - Geo location lt[] table parameter was empty, process not executed!, Line 110');
                        }

                    }


                    if ($successful === false) {
                        ChannelLog::write("event", time() . ' Xirrius : Writing Json content to the file -' . $filename . ' failed! Line 117');
                    }
                }

                $this->processJsonPayload(config('directory.payload_dir'), $this->location);

                return response()->json($this->request);

            } else {
                ChannelLog::write("event", time() . ' Xirrius : Missing post: Request stream data: Line 125');
            }

        } catch (Exception $e) {

            ChannelLog::write('event', $e->getMessage());
        }
    }


    protected function processJsonPayload($path, $locationName)
    {

        try {

            $handle = opendir($path);


            while (false !== ($entry = readdir($handle))) {

                if ($entry != '.' && $entry != '..') {

                    $filepath = "{$path}/{$entry}";


                    if (!empty($filepath)) {

                        $filename = $entry;
                        event("xirrius.getLatestCreatedFile", $filename);

                        /* File moving logic*/
                        $now = Carbon::now()->timestamp;

                        $prevTimestamp = (intval($now) - config('directory.interval_limit'));
                        $fileWriteTimestamp = (intval($now) - config('directory.live_period'));
                        // print('current - '.intval($now).' prev time - '. $prevTimestamp);


                        $filename = $this->file_name;
                        event("xirrius.getLatestCreatedFile", $filename);

                        /* File moving logic end*/

                        $tempPath = config('directory.pending_dir') . '/' . $filename;


                        event("xirrius.inPendingState", $filename);


                        $jsonData = json_decode(file_get_contents($filepath), TRUE);

                        $processArray = array();

                        if (isset($jsonData['ld'][0])) {

                            $processArray['lt_values'] = json_decode($jsonData['ld'][0], TRUE);

                            # open the temporary file to write to
                            if (!empty($processArray['lt_values']['lt'])) {

                                $fp = gzopen($tempPath, 'a');
                                # write the working file's contents

                                $i = 0;

                                $successful = false;

                                if (isset($processArray['lt_values']['lt'])) {


                                    foreach ($processArray['lt_values']['lt'] as $each) {

                                        $i++;

                                        $processArray['data'][$i]['ln'] = snake_case(trim($jsonData['ln'][0], '"'));

                                        foreach ($each as $key => $value) {


                                            if (in_array($key, [
                                                'si', 'ap', 'cn', 'ot', 'ct', 'cf', 'ih', 'sl', 'sh', 'so', 'sc'

                                            ])) {
                                                $processArray['data'][$i]['vn'] = $processArray['lt_values']['vn'];
                                                $processArray['data'][$i]['ma'] = $processArray['lt_values']['ma'];
                                                $processArray['data'][$i]['mc'] = $processArray['lt_values']['mc'];
                                                $processArray['data'][$i][$key] = $value;
                                            }
                                        }

                                    }

                                    if (isset($processArray['data'])) {

                                        foreach ($processArray['data'] as $key => $each) {

                                            $i++;

                                            $successful = fputcsv($fp, $each, "\t");  # tab delimited
                                        }
                                    }
                                }

                                fclose($fp);

                                // //$successful = fputcsv ($fp, $csvDataRow);     # comma delimited
                                if ($successful === false) {

                                    event("event", time() . ' File unable to write!');

                                    //return false;

                                } else {

                                    if (file_exists($filepath)) {
                                        unlink($filepath); # writing the file failed,  remove the temporary file
                                        event("xirrius.inPendingState", $filename . ' - Temp/new Directory Json file deleted!');
                                    }
                                }

                                //event("xirrius.modifyWaiting", print_r($each));

                                //return true;


                            } else {
                                event("event", time() . ' File LT[] parameter was empty or unable to load!');
                            }


                        } else {

                            if (isset($jsonData['xps_ld']['lt']) && isset($jsonData['xps_ld']['id'])) {


                                # open the temporary file to write to
                                if (!empty($jsonData['xps_ld']['lt'])) {

                                    $fp = gzopen($tempPath, 'a');
                                    # write the working file's contents

                                    $i = 0;

                                    $successful = false;

                                    if (isset($jsonData['xps_ld']['lt'])) {


                                        foreach ($jsonData['xps_ld']['lt'] as $each) {

                                            $i++;


                                            $processArray['data'][$i]['time_stamp'] = $jsonData['xps_ld']['time-stamp'];

                                            foreach ($each as $key => $value) {

                                                /**
                                                 *in case if any one wants to reffer the previous version with ordering
                                                 * cho $key.' :'.  $value. PHP_EOL;
                                                 *
                                                 * if(in_array($key, [
                                                 * 'as', 'mp', 'pn', 'px', 'py', 'pz', 'si'
                                                 *
                                                 * ])) {
                                                 *
                                                 *
                                                 *
                                                 * $processArray['data'][$i]['si'] =  $each['si'];
                                                 * $processArray['data'][$i]['px'] =  $each['px'];
                                                 * $processArray['data'][$i]['py'] =  $each['py'];
                                                 * $processArray['data'][$i]['pz'] =  $each['pz'];
                                                 * $processArray['data'][$i]['pn'] =  $each['pn'];
                                                 * $processArray['data'][$i]['as'] =  $each['as'];
                                                 * $processArray['data'][$i]['mp'] =  $each['mp'];
                                                 *
                                                 * }
                                                 *
                                                 */

                                                $order = ["time_stamp", "si", "px", "py", "pz", "pn", "as", "apMac", "mp"];

                                                if (in_array($key, $order)) {
                                                    $processArray['data'][$i][$key] = $each[$key];

                                                    uksort($processArray['data'][$i], function ($key1, $key2) use ($order) {
                                                        return (array_search($key1, $order) > array_search($key2, $order));
                                                    });


                                                }
                                            }


                                        }

                                        if (isset($processArray['data'])) {

                                            foreach ($processArray['data'] as $key => $each) {

                                                $i++;

                                                $successful = fputcsv($fp, $each, "\t");  # tab delimited
                                            }
                                        }
                                    }

                                    fclose($fp);

                                    // //$successful = fputcsv ($fp, $csvDataRow);     # comma delimited
                                    if ($successful === false) {

                                        event("event", time() . ' File unable to write!');

                                        //return false;

                                    } else {

                                        if (file_exists($filepath)) {
                                            unlink($filepath); # writing the file failed,  remove the temporary file
                                            event("xirrius.inPendingState", $filename . ' - Temp/new Directory Json file deleted!');
                                        }
                                    }

                                    //event("xirrius.modifyWaiting", print_r($each));

                                    //return true;


                                } else {
                                    event("event", time() . ' File directory was empty or unable to load!');
                                }


                            } else {
                                event("event", time() . ' Unable to identify payload content!');
                            }
                        }

                    }


                    $this->moveFilefromPending(config('directory.pending_dir'), $fileWriteTimestamp);

                } else {
                    event("event", time() . ' File path was empty!');
                }
            }

        } catch (Exception $e) {

            ChannelLog::write('event', $e->getMessage());
        }
    }


    public function failed(Exception $e)
    {
        // Send user notification of failure, etc...
        try {
            $failed = FailedJob::select('id')->orderBy('failed_at', 'desc')->first();
            $jobId = $failed->id;
            ChannelLog::write("failed_job_event", time() . ' ', ['message' => $e->getMessage(), 'jobId' => $jobId, 'time' => date('Y-m-d h:i:s', time())]);

        } catch (Exception $e) {

            ChannelLog::write('event', $e->getMessage());
        }
    }

}
