<?php

namespace App\Listeners;

use ChannelLog;
use App\Events\ListenXirrius;

class ListningEventFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ListenXirrius  $event
     * @return void
     */
    public function handle(ListenXirrius $event)
    {
    
    }

    public function subscribe( $events ) {
            $events->listen( 'xirrius.listing', 'App\Listeners\ListningEventFired@onListning' );
            $events->listen( 'xirrius.modifyWaiting', 'App\Listeners\ListningEventFired@onWriteWaiting' );
            $events->listen( 'xirrius.modifyCompleted', 'App\Listeners\ListningEventFired@onWriteCompleted' );
            $events->listen( 'xirrius.getLatestModifiedFile', 'App\Listeners\ListningEventFired@getLatestModifiedFile' );
            $events->listen( 'xirrius.getLatestCreatedFile', 'App\Listeners\ListningEventFired@getLatestCreatedFile' );
            $events->listen( 'xirrius.errorMessage', 'App\Listeners\ListningEventFired@recordError' );
            $events->listen( 'xirrius.inPendingState', 'App\Listeners\ListningEventFired@movedToPending' );
            $events->listen( 'xirrius.inReadyState', 'App\Listeners\ListningEventFired@movedToReady' );
    }
    

    public function onListning($filename = ''){
        ChannelLog::write('event', 'Api: Listining : ',['filename' => $filename]);
    }

    public function onWriteWaiting($filename = ''){
         ChannelLog::write('event','Waiting to append file :',['filename' => $filename]);
    }

    public function onWriteCompleted($filename = '', $data = ''){
         ChannelLog::write('event', 'File has been appended : ',['filename' => $filename]);
    }

    public function getLatestModifiedFile($filename = ''){
         ChannelLog::write('event', 'Last modified file :',['filename' => $filename]);
    }

    public function getLatestCreatedFile($filename = ''){
        ChannelLog::write('event', 'Last created file :',['filename' => $filename]);
    }

    public function recordError($e){
        ChannelLog::write('event', 'Error occured: ',['error' => $e]);
    }

    public function movedToPending($filename = ''){
        ChannelLog::write('event', 'Created to pending folder - file :',['filename' => $filename]);
    }

    public function movedToReady($filename = ''){
        ChannelLog::write('event', 'Moved to ready folder - file :',['filename' => $filename]);
    }
}
