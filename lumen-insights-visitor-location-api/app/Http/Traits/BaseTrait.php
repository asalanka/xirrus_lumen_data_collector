<?php
namespace App\Http\Traits;
use App\Events\ListenXirrius;
use Carbon\Carbon;
use App\Contracts\Facades\ChannelLog as Log;

trait BaseTrait {


    protected function writeOutput($csvData, $locationName){
	    try{


	    	$row_count = 0;
	  		
	    	# write the data out to file
			if (count ($csvData)) {

				if(!empty($locationName)){
					$now = Carbon::now()->timestamp;

                    $prevTimestamp = (intval($now) - config('directory.interval_limit'));
                    $fileWriteTimestamp = (intval($now) - config('directory.live_period'));

		            // print('current - '.intval($now).' prev time - '. $prevTimestamp);

					$this->moveFilefromPending(config('directory.pending_dir'), $fileWriteTimestamp);
	 		 		$filename = $this->getFileNameByLocationTimeStamp(intval($now),  $locationName, config('directory.interval_limit'),config('directory.pending_dir'));
		            //dd($filename);

					if(empty($filename)){
						$filename = $this->generateFilename($locationName);
						event("xirrius.getLatestCreatedFile",$filename);
					}
					
					$tempPath = config('directory.pending_dir').'/'.$filename;
					
					# open the temporary file to write to
					if ($fp = fopen ($tempPath, 'a')) {
						# write the working file's contents
						event("xirrius.inPendingState", $filename);
						$data = json_decode($csvData['ld'], TRUE);

						event("xirrius.modifyWaiting", $filename);

						$processArray = array();

						$i = 0;
					
						foreach( $data['lt'] as $each){

							$i++;
							$processArray[$i]['data']['ln'] = trim(str_slug($csvData['ln']), '"');
							foreach ($each as $key => $value) {
								if(in_array($key, [
								  'si', 'ap', 'cn', 'ot', 'ct', 'cf', 'ih', 'sl', 'sh','so','sc'

								])) {
									$processArray[$i]['data']['vn'] = $data['vn'];
                                    $processArray[$i]['data']['ma'] = $data['ma'];
                                    $processArray[$i]['data']['mc'] = $data['mc'];
									$processArray[$i]['data'][$key] = $value;	
								}
							}
							
						}

							$row_count = 0;

							foreach ($processArray as $key => $eachElement) {

								foreach ($eachElement as $csvDataRow) {

									$row_count++;

									$successful = fputcsv($fp, $csvDataRow, "\t");	# tab delimited

									//$successful = fputcsv ($fp, $csvDataRow);		# comma delimited
									if ($successful === false) {
										fclose ($fp);
										unlink ($tempPath);	# writing the file failed,  remove the temporary file

										return false;
									}
								}
							}

							event("xirrius.getLatestModifiedFile",$filename);

							# close the working file
							if (!fclose ($fp)) {
								fclose ($fp);	# try to close the file again?
								unlink ($tempPath);	# writing the file failed,  remove the temporary file

								return false;
							}



							return true;
					}

				}else{
					event("xirrius.errorMessage", time().' AP Lcation Name was empty!');
				}
	                        
	           
			}else{
				event("xirrius.errorMessage", time().' AP Stream data was empty!');
			}

			return true;	# no data to write (still successful)

	    }catch(Exception $e){
	    	 event("xirrius.errorMessage", $e);
	    }	
    }

    protected function generateFilename ($locationName) 
    {
    	return 'obs.'.date('Ymd').'-'.date('His').'-'. str_slug(strtolower($locationName));
    }


    protected function getFileNameByLocationTimeStamp($timeStamp, $locationName, $intervalTime,$path){
    	$latest_filename = '';

    	for($i = 0; $i <= $intervalTime; $i++){ 
    		$filenames[] = 'obs.'.date('Ymd',$timeStamp).'-'.date('His',($timeStamp-$i)).'-'. str_slug(strtolower($locationName));
    	}

    	$d = dir($path);
		while (false !== ($entry = $d->read())) {
		  $filepath = "{$path}/{$entry}";

			  // could do also other checks than just checking whether the entry is a file

			    if(!empty($filenames)){
		    		if(in_array($entry, $filenames)){

		    			$latest_filename = $entry;

		    	}

			}

	    }

	    return $latest_filename;

	}

    protected function moveFilefromPending($path, $fileWriteTimestamp){


		$d = dir($path);
		while (false !== ($entry = $d->read())) {
		  $filepath = "{$path}/{$entry}";

		  // could do also other checks than just checking whether the entry is a file  (Endpoint Time gap * $fileWriteTimestamp)
		  if (is_file($filepath) && filectime($filepath) > $fileWriteTimestamp) {
		    //$latest_ctime = filectime($filepath);
		    $latest_filename = $entry;
		 
		  }else{

			    if($entry != '.' && $entry != '..'){

                    $filesize = filesize(config('directory.pending_dir').'/'.$entry);
			    	

			    	if($filesize > 0){
                        Log::write('event', config('directory.ready_dir'));
                        rename(config('directory.pending_dir').'/'.$entry, config('directory.ready_dir').'/'.$entry);
                        event("xirrius.inReadyState",config('directory.ready_dir').'/'. $entry);

			    	}else{
			    		  if(file_exists(config('directory.pending_dir').'/'.$entry)){
                                unlink(config('directory.pending_dir').'/'.$entry); # writing the file failed,  remove the temporary file
                                event("xirrius.inPendingState", config('directory.payload_dir').'/'.$entry.' - '.$filesize.'byte size pending Directory file deleted!');event("xirrius.inPendingState", config('directory.pending_dir').'/'.$entry.' - '.$filesize.'byte size pending Directory file deleted!');
                            }
			    	}

			    }
            }
		}

    }

}
