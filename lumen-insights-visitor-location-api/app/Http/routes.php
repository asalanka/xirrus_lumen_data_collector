<?php

$app->get('/', function () use ($app) {
    return $app->version();
});


// http://location-api.invigorinsights.com/locations/xrs/zoo-hat
$app->post('locations/xps/{location}', 'xirrius\LocationCollectorController@serviceCollectPayload');
