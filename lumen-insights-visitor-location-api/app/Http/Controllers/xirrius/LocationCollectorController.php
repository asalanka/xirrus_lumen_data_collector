<?php

namespace App\Http\Controllers\xirrius;

use Queue;
use Exception;
use ChannelLog;
use App\Jobs\CapturePayload;
use Illuminate\Http\Request;
use App\Http\Traits\BaseTrait;
use App\Http\Controllers\Controller;


class LocationCollectorController extends Controller
{
    use BaseTrait;

    public function serviceCollectPayload(Request $request, $location)
    {

        // capture the payload
        // capture the location
        // delicating the job to CapturePayload job class
        try {

            $file_name = $this->generateFilenameSnakeCase(trim($location, '"'));

            ChannelLog::write('event','FileName['.$file_name.']-Timestamp['.time().']-Request['.json_encode($request->all(), true).']-End');

            ChannelLog::write('event', 'Writing payload/response in to the file!');
            //dd(file_get_contents("php://input"));
            $api_job = (new CapturePayload(json_encode($request->all(), true), trim($location, '"'), $file_name))->onQueue('apijob');

            dispatch($api_job);

            ChannelLog::write('event', 'Request completed');

        } catch (Exception $e) {

            ChannelLog::write('event', 'Request completed  with error :' . $e);
        }
    }

    protected function generateFilenameSnakeCase($locationName)
    {
        return 'obs.' . date('Ymd') . '-' . date('His') . '-' . snake_case(strtolower(str_replace('%20', '_', $locationName)));
    }


}

