<?php

namespace App\Providers;

use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //Facade to Object binding
        $this->app->bind('chanellog', 'App\Helpers\ChannelWriter');
    }


    public function boot()
    {
        Queue::before(function (JobProcessing $event) {
            //
        });

        Queue::after(function (JobProcessed $event) {
            //
        });
    }

}
